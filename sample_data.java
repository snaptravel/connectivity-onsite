import java.io.*;
import java.util.*;

/** Step 1 */
class Solution {

  public static void main(String[] args) {
    // Sample request (only a sample)
    int userId = 488;
    Request request = new Request(userId, Arrays.asList(39, 66, 22));

    // Call the "GET request"
    List<HotelDto> result = handleRequest(request);

    // Print the result
    for (HotelDto dto : result) {
      System.out.println(
        "hotel_id: " + dto.hotelId +
        ", snaptravelPrice: " + dto.snaptravelPrice +
        ", retailPrice: " + dto.retailPrice
      );
    }
  }

  public static List<Hotel> getHotelList() {
    List<Hotel> hotels = new ArrayList<>();
    Hotel hotel = new Hotel(39, new Price(117.99, 132.45));
    hotels.add(hotel);
    hotel = new Hotel(66, new Price(134.99, 113.45));
    hotels.add(hotel);
    hotel = new Hotel(90, new Price(194.99, 196.99));
    hotels.add(hotel);
    hotel = new Hotel(22, new Price(167.99, 112.45));
    hotels.add(hotel);

    return hotels;
  }

  public static List<HotelDto> handleRequest(Request request) {
    // hotel data from "some" data source
    List<Hotel> hotels = getHotelList();

    // Get prices from hotels using data in the request

    return new ArrayList<HotelDto>();
  }
}

class Request {
  public int userId;
  public List<Integer> hotelIds;

  public Request(int userId, List<Integer> hotelIds) {
    this.userId = userId;
    this.hotelIds = hotelIds;
  }
}

class Hotel {
  public int hotelId;
  public Price price;
  public String amenities;
  public String numReviews;

  public Hotel(int hotelId, Price price) {
    this.hotelId = hotelId;
    this.price = price;
  }
}

class Price {
  public double snaptravel;
  public double retail;

  public Price(double snaptravel, double retail) {
    this.snaptravel = snaptravel;
    this.retail = retail;
  }
}

class HotelDto {
  public int hotelId;
  public double snaptravelPrice;
  public double retailPrice;

  public HotelDto(int hotelId, double snaptravelPrice, double retailPrice) {
    this.hotelId = hotelId;
    this.snaptravelPrice = snaptravelPrice;
    this.retailPrice = retailPrice;
  }
}
/***************/

/** Step 2.1 **/
class HotelDto {
  public int hotelId;
  public double snaptravelPrice;
  public double retailPrice;
  public double businessTravellerPrice;

  public HotelDto(int hotelId, double snaptravelPrice, double retailPrice, double businessTravellerPrice) {
    this.hotelId = hotelId;
    this.snaptravelPrice = snaptravelPrice;
    this.retailPrice = retailPrice;
    this.businessTravellerPrice = businessTravellerPrice;
  }
}

// and add this to the Print loop in main():
// + ", businessTravellerPrice: " + dto.businessTravellerPrice

/** Step 2.2 **/
class ExperimentBucket {
  public int markupPercent;
  public ExperimentBucket(int markupPercent) {
    this.markupPercent = markupPercent;
  }
}

/** Driving code - main() **/
List<ExperimentBucket> experiment = new ArrayList<>();
ExperimentBucket bucket = new ExperimentBucket(10);
experiment.add(bucket);
bucket = new ExperimentBucket(20);
experiment.add(bucket);
bucket = new ExperimentBucket(50);
experiment.add(bucket);

/** Step 2.3 **/
class ExperimentBucket {
  public int markupPercent;
  public int weight;
  public ExperimentBucket(int markupPercent, int weight) {
    this.markupPercent = markupPercent;
    this.weight = weight;
  }
}

/** Driving code - main() **/
List<ExperimentBucket> experiment = new ArrayList<>();
ExperimentBucket bucket = new ExperimentBucket(10, 5);
experiment.add(bucket);
bucket = new ExperimentBucket(20, 2);
experiment.add(bucket);
bucket = new ExperimentBucket(50, 3);
experiment.add(bucket);