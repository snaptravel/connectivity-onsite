let sample_normalized_hotel_data = () => {
  return [{
    'id': 39,
    'hotel_name': 'The Colorado Overlook Hotel',
    'num_reviews': 6,
    'address': '132 Long Alley Rd, Very Large City',
    'num_stars': 2,
    'amenities': [],
    'image_url': 'https://images.trvl-media.com/hotels/1000000/10000/1500/1443/d73823db_b.jpg',
    'price': {
      'snaptravel': 117.99,
      'retail': 132.45
    }
  }, {
    'id': 66,
    'hotel_name': 'The Pennsylvanian Factory Hotel',
    'num_reviews': 4491,
    'address': '32 West 33rd Street, Very Large City',
    'num_stars': 3,
    'amenities': [
        'Wi-Fi',
        'Pool',
        'Breakfast'
    ],
    'image_url': 'https://images.trvl-media.com/hotels/2000000/1070000/1062900/1062879/1062879_24_b.jpg',
    'price': {
      'snaptravel': 135.99,
      'retail': 132.45
    }
  }, {
    'id': 90,
    'hotel_name': 'The Grand Budapest Hotel',
    'num_reviews': 1372,
    'address': '231 Boylston St, Very Large City',
    'num_stars': 5,
    'amenities': [
        'Parking',
        'Pool'
    ],
    'image_url': 'https://images.trvl-media.com/hotels/1000000/910000/904900/904821/904821_178_b.jpg',
    'price': {
      'snaptravel': 194.99,
      'retail': 196.99
    }
  }, {
    'id': 22,
    'hotel_name': 'The Marriot Fruit Tree Hotel',
    'num_reviews': 1293,
    'address': '12 Wall Street, Very Large City',
    'num_stars': 3,
    'amenities': [
        'Wi-Fi',
        'Breakfast'
    ],
    'image_url': 'https://images.trvl-media.com/hotels/1000000/470000/460500/460434/460434_73_b.jpg',
    'price': {
      'snaptravel': 94.99,
      'retail': 96.99
    }
  }]
}

let sample_request_body = () => {
  return {
    'user_id': 487,
    'hotel_ids': [39, 66, 22],
    'source': 'new_bt'
  }
}

const EXPERIMENT = { 
  'name' : 'business_traveller_bucket',
  'distribution' : [
      {
        'percent' : 10
      },
      {
        'percent' : 20
      },
      {
        'percent' : 50
      }]
}
